//
//  GHCategories.m
//
//  Created by Alex Martin on 11/26/13.
//  Copyright (c) 2013 Alex Martin. All rights reserved.
//

#import "GHCategories.h"
#import <objc/runtime.h>
#import <CommonCrypto/CommonDigest.h>

@interface NSCBAlertWrapper : NSObject

@property (copy) void(^completionBlock)(UIAlertView *alertView, NSInteger buttonIndex);

@end

@implementation NSCBAlertWrapper

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [alertView resignFirstResponder];
    if (self.completionBlock)
        self.completionBlock(alertView, buttonIndex);
}

- (void)alertViewCancel:(UIAlertView *)alertView {
    [alertView resignFirstResponder];
    // Just simulate a cancel button click
    if (self.completionBlock)
        self.completionBlock(alertView, alertView.cancelButtonIndex);
}

@end

@implementation NSString (GHCategories)

- (NSInteger)getFontSizeToFitSize:(CGSize)size {
    // Try all font sizes from largest to smallest font
    int maxFontSize = 300;
    int minFontSize = 1;
    
    return [self binarySearchForFontSizeForText:self withMinFontSize:minFontSize withMaxFontSize:maxFontSize withSize:size];
}

- (NSInteger)binarySearchForFontSizeForText:(NSString *)text withMinFontSize:(NSInteger)minFontSize withMaxFontSize:(NSInteger)maxFontSize withSize:(CGSize)size {
    // If the sizes are incorrect, return 0, or error, or an assertion.
    if (maxFontSize < minFontSize)
        return 0;
    
    // Find the middle
    NSInteger fontSize = (minFontSize + maxFontSize) / 2;
    // Create the font
    UIFont *font = [UIFont boldSystemFontOfSize:fontSize];
    // Create a constraint size with max height
    CGSize constraintSize = CGSizeMake(size.width, MAXFLOAT);
    // Find label size for current font size
    CGSize labelSize = [text sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    
    // EDIT:  The next block is modified from the original answer posted in SO to consider the width in the decision. This works much better for certain labels that are too thin and were giving bad results.
    if( labelSize.height >= (size.height+10) && labelSize.width >= (size.width + 10) && labelSize.height <= (size.height) && labelSize.width <= (size.width) ) {
        //NSLog(@"'%@' LabelSize: (%f x %f) Font imprint: (%f x %f)", text, labelSize.width, labelSize.height, size.width, size.height);
        return fontSize;
    } else if( labelSize.height > size.height || labelSize.width > size.width)
        return [self binarySearchForFontSizeForText:text withMinFontSize:minFontSize withMaxFontSize:fontSize-1 withSize:size];
    else
        return [self binarySearchForFontSizeForText:text withMinFontSize:fontSize+1 withMaxFontSize:maxFontSize withSize:size];
}

- (NSString *)MD5 {
    const char *str = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (int)strlen(str), result);
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]];
}

-(NSString *)sha1 {
    const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
	NSData *data 	 = [NSData dataWithBytes:cstr length:self.length];
    
	uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
	CC_SHA1(data.bytes, (int)data.length, digest);
    
	NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
	for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
		[output appendFormat:@"%02x", digest[i]];
    
	return output;
}

-(NSString *)reverse {
	NSMutableString *reverse = [NSMutableString new];
    for (int i = self.length - 1; i >= 0; i--) [reverse appendString:[NSString stringWithFormat:@"%c", [self characterAtIndex:i]]];
    return reverse;
}

-(NSUInteger)countWords {
    
    __block NSUInteger wordCount = 0;
    [self enumerateSubstringsInRange:NSMakeRange(0, self.length)
                             options:NSStringEnumerationByWords
                          usingBlock:^(NSString *character, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                              wordCount++;
                          }];
    return wordCount;
}

-(NSString *)stringByStrippingWhitespace {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

-(NSString *)substringFrom:(NSInteger)from to:(NSInteger)to {
    NSString *rightPart = [self substringFromIndex:from];
    return [rightPart substringToIndex:to-from];
}

-(NSString *)URLEncode {
    NSString *output = [NSString string];
    output = [output stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return output;
    /*
     CFStringRef encoded = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
     (__bridge CFStringRef)self,
     NULL,
     CFSTR(":/?#[]@!$&'()*+,;="),
     kCFStringEncodingUTF8);
     return [NSString stringWithString:(__bridge_transfer NSString *)encoded];
     */
}

-(NSString *)URLDecode {
    
    CFStringRef decoded = CFURLCreateStringByReplacingPercentEscapes( kCFAllocatorDefault,
                                                                     (__bridge CFStringRef)self,
                                                                     CFSTR(":/?#[]@!$&'()*+,;=") );
    return [NSString stringWithString:(__bridge_transfer NSString *)decoded];
}

-(NSString *)CamelCaseToUnderscores:(NSString *)input {
    
    NSMutableString *output = [NSMutableString string];
    NSCharacterSet *uppercase = [NSCharacterSet uppercaseLetterCharacterSet];
    for (NSInteger idx = 0; idx < [input length]; idx += 1) {
        unichar c = [input characterAtIndex:idx];
        if ([uppercase characterIsMember:c]) {
            [output appendFormat:@"%s%C", (idx == 0 ? "" : "_"), (unichar)(c ^ 32)];
        } else {
            [output appendFormat:@"%C", c];
        }
    }
    return output;
}

-(NSString *)UnderscoresToCamelCase:(NSString*)underscores {
    
    NSMutableString *output = [NSMutableString string];
    BOOL makeNextCharacterUpperCase = NO;
    for (NSInteger idx = 0; idx < [underscores length]; idx += 1) {
        unichar c = [underscores characterAtIndex:idx];
        if (c == '_') {
            makeNextCharacterUpperCase = YES;
        } else if (makeNextCharacterUpperCase) {
            [output appendString:[[NSString stringWithCharacters:&c length:1] uppercaseString]];
            makeNextCharacterUpperCase = NO;
        } else {
            [output appendFormat:@"%C", c];
        }
    }
    return output;
}

-(NSString *)CapitalizeFirst {
    if ([self length] == 0) {
        return self;
    }
    
    if ([self contains:@" "]) {
        NSArray *split = [self componentsSeparatedByString:@" "];
        NSString *newStr = @"";
        for (NSString *item in split) newStr = [newStr stringByAppendingFormat:@"%@ ", [item CapitalizeFirst]];
        newStr = [newStr stringByStrippingWhitespace];
        return newStr;
    } else {
        return [[self lowercaseString] stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[self substringWithRange:NSMakeRange(0, 1)] capitalizedString]];
    }
}

- (NSString *)removeString:(NSString *)remove {
    return [self stringByReplacingOccurrencesOfString:remove withString:@""];
}

- (NSString *)mileageToStr {
    if (self == nil || [self isEqualToString:@""]) return @"";
    
    int mileNumber = [self intValue];
    NSNumberFormatter *form = [NSNumberFormatter new];
    [form setNumberStyle:NSNumberFormatterDecimalStyle];
    
    return [form stringFromNumber:[NSNumber numberWithInt:mileNumber]];
}

- (NSString *)strToMileage {
    return [[self stringByReplacingOccurrencesOfString:@"," withString:@""] stringByAppendingString:@".0"];
}

-(BOOL)isBlank {
    return self == nil || [[self stringByStrippingWhitespace] isEqualToString:@""];
}

-(BOOL)contains:(NSString *)string {
    NSRange range = [self rangeOfString:string];
    return (range.location != NSNotFound);
}

- (NSString *)getFirstWordsWithMaxCharacters:(int)maxChars {
    if (self == nil || [self isBlank] || self.length > maxChars) return self;
    
    //split the string into different words
    NSArray *words = [self componentsSeparatedByString:@" "];
    NSMutableString *finalString = [NSMutableString new];
    
    //add the words to the final string if the final string is under the count
    for (int i = 0; i < words.count; i++) {
        NSString *thisWord = [words objectAtIndex:i];
        if ([thisWord isBlank]) continue;
        if (finalString.length + thisWord.length >= maxChars) break;
        
        //remove any special characters and add it to the final string
        [finalString appendString:[thisWord stringByStrippingSpecialCharacters]];
    }
    
    return finalString;
}

- (NSString *)stringByStrippingSpecialCharacters {
    const NSString *allow = @"abcdefghijklmnopqrstuvwxyz0123456789";
    NSMutableString *newString = [NSMutableString new];
    for (int i = 0; i < self.length; i++) {
        NSString *c = [NSString stringWithFormat:@"%c", [self characterAtIndex:i]];
        if ([allow contains:c]) {
            [newString appendString:c];
        }
    }
    return newString;
}

@end

@implementation NSUserDefaults (GHCategories)

+ (void)saveObject:(id)object forKey:(NSString *)key {
    NSUserDefaults *defaults = [self standardUserDefaults];
    [defaults setObject:object forKey:key];
    [defaults synchronize];
}

+ (id)retrieveObjectForKey:(NSString *)key {
    return [[self standardUserDefaults] objectForKey:key];
}

+ (void)deleteObjectForKey:(NSString *)key {
    NSUserDefaults *defaults = [self standardUserDefaults];
    [defaults removeObjectForKey:key];
    [defaults synchronize];
}

@end

@implementation UIImage (GHCategories)
#define DEGREES_RADIANS(angle) ((angle) / 180.0 * M_PI)

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees {
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.size.width, self.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(DEGREES_RADIANS(degrees));
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, DEGREES_RADIANS(degrees));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-self.size.width / 2, -self.size.height / 2, self.size.width, self.size.height), [self CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end

@implementation UIView (GHCategories)

- (UIView *)viewRotatedByDegrees:(CGFloat)degrees {
    self.transform = CGAffineTransformMakeRotation(DEGREES_RADIANS(degrees));
    return self;
}

- (UIView *)resignSubviewFirstResponders {
    if ([self isFirstResponder]) {
        [self resignFirstResponder];
        return self;
    }
    
    for(UIView *subview in self.subviews) {
        UIView *result = [subview resignSubviewFirstResponders];
        if (result) return result;
    }
    
    return nil;
}

@end

@implementation UIAlertView (GHCategories)

+ (UIAlertView *)showOkAlertWithTitle:(NSString *)title andMessage:(NSString *)message {
    UIAlertView *v = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    dispatch_async(dispatch_get_main_queue(), ^{ [v show]; });
    return v;
}

static const char kNSCBAlertWrapper;
- (void)showWithCompletion:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completion {
    NSCBAlertWrapper *alertWrapper = [[NSCBAlertWrapper alloc] init];
    alertWrapper.completionBlock = completion;
    self.delegate = alertWrapper;
    
    // Set the wrapper as an associated object
    objc_setAssociatedObject(self, &kNSCBAlertWrapper, alertWrapper, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    // Show the alert as normal
    [self show];
}

@end

@implementation NSDate (GHCategories)

#define SECONDS_PER_MINUTE 60.0
#define SECONDS_PER_HOUR   3600.0
#define SECONDS_PER_DAY    86400.0
#define SECONDS_PER_MONTH  2592000.0
#define SECONDS_PER_YEAR   31536000.0

- (NSString *)formatWithString:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *string = [formatter stringFromDate:self];
    return string;
}

- (NSString *)formatWithStyle:(NSDateFormatterStyle)style {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:style];
    [formatter setTimeStyle:style];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *string = [formatter stringFromDate:self];
    return string;
}

- (NSString *)distanceOfTimeInWords {
    return [self distanceOfTimeInWords:[[NSDate date] dateByAddingTimeInterval:60]];
}

- (NSString *)distanceOfTimeInWords:(NSDate *)date {
    NSString *Ago      = NSLocalizedString(@"ago", @"Denotes past dates");
    NSString *FromNow  = NSLocalizedString(@"from now", @"Denotes future dates");
    NSString *LessThan = NSLocalizedString(@"Less than", @"Indicates a less-than number");
    NSString *About    = NSLocalizedString(@"About", @"Indicates an approximate number");
    NSString *Over     = NSLocalizedString(@"Over", @"Indicates an exceeding number");
    NSString *Almost   = NSLocalizedString(@"Almost", @"Indicates an approaching number");
    //NSString *Second   = NSLocalizedString(@"second", @"One second in time");
    NSString *Seconds  = NSLocalizedString(@"seconds", @"More than one second in time");
    NSString *Minute   = NSLocalizedString(@"minute", @"One minute in time");
    NSString *Minutes  = NSLocalizedString(@"minutes", @"More than one minute in time");
    NSString *Hour     = NSLocalizedString(@"hour", @"One hour in time");
    NSString *Hours    = NSLocalizedString(@"hours", @"More than one hour in time");
    NSString *Day      = NSLocalizedString(@"day", @"One day in time");
    NSString *Days     = NSLocalizedString(@"days", @"More than one day in time");
    NSString *Month    = NSLocalizedString(@"month", @"One month in time");
    NSString *Months   = NSLocalizedString(@"months", @"More than one month in time");
    NSString *Year     = NSLocalizedString(@"year", @"One year in time");
    NSString *Years    = NSLocalizedString(@"years", @"More than one year in time");
    
    NSTimeInterval since = [self timeIntervalSinceDate:date];
    
    NSString *direction = since <= 0.0 ? Ago : FromNow;
    since = fabs(since);
    
    int seconds   = (int)since;
    int minutes   = (int)round(since / SECONDS_PER_MINUTE);
    int hours     = (int)round(since / SECONDS_PER_HOUR);
    int days      = (int)round(since / SECONDS_PER_DAY);
    int months    = (int)round(since / SECONDS_PER_MONTH);
    int years     = (int)floor(since / SECONDS_PER_YEAR);
    int offset    = (int)round(floor((float)years / 4.0) * 1440.0);
    int remainder = (minutes - offset) % 525600;
    
    int number;
    NSString *measure;
    NSString *modifier = @"";
    
    switch (minutes) {
        case 0 ... 1:
            measure = Seconds;
            switch (seconds) {
                case 0 ... 4:
                    number = 5;
                    modifier = LessThan;
                    break;
                case 5 ... 9:
                    number = 10;
                    modifier = LessThan;
                    break;
                case 10 ... 19:
                    number = 20;
                    modifier = LessThan;
                    break;
                case 20 ... 39:
                    number = 30;
                    modifier = About;
                    break;
                case 40 ... 59:
                    number = 1;
                    measure = Minute;
                    modifier = LessThan;
                    break;
                default:
                    number = 1;
                    measure = Minute;
                    modifier = About;
                    break;
            }
            break;
        case 2 ... 44:
            number = minutes;
            measure = Minutes;
            break;
        case 45 ... 89:
            number = 1;
            measure = Hour;
            modifier = About;
            break;
        case 90 ... 1439:
            number = hours;
            measure = Hours;
            modifier = About;
            break;
        case 1440 ... 2529:
            number = 1;
            measure = Day;
            break;
        case 2530 ... 43199:
            number = days;
            measure = Days;
            break;
        case 43200 ... 86399:
            number = 1;
            measure = Month;
            modifier = About;
            break;
        case 86400 ... 525599:
            number = months;
            measure = Months;
            break;
        default:
            number = years;
            measure = number == 1 ? Year : Years;
            if (remainder < 131400) {
                modifier = About;
            } else if (remainder < 394200) {
                modifier = Over;
            } else {
                ++number;
                measure = Years;
                modifier = Almost;
            }
            break;
    }
    if ([modifier length] > 0) {
        modifier = [modifier stringByAppendingString:@" "];
    }
    return [NSString stringWithFormat:@"%@%d %@ %@", modifier, number, measure, direction];
}

- (NSArray *)getFirstAndLastDatesOfDay {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *startOfPeriod;
    NSDate *endOfPeriod;
    NSTimeInterval interval;
    [cal rangeOfUnit:NSDayCalendarUnit startDate:&startOfPeriod interval:&interval forDate:self];
    //holds 00:00:01
    
    endOfPeriod = [startOfPeriod dateByAddingTimeInterval:interval-1];
    // holds 23:59:59
    return @[startOfPeriod, endOfPeriod];
}

- (NSArray *)getFirstAndLastDatesOfMonth {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *startOfPeriod;
    NSDate *endOfPeriod;
    NSTimeInterval interval;
    [cal rangeOfUnit:NSMonthCalendarUnit startDate:&startOfPeriod interval:&interval forDate:self];
    //startOfWeek holds now the first day of the month
    
    endOfPeriod = [startOfPeriod dateByAddingTimeInterval:interval-1];
    // holds 23:59:59 of last day in month.
    return @[startOfPeriod, endOfPeriod];
}

//http://stackoverflow.com/questions/11681815/current-week-start-and-end-date
- (NSArray *)getFirstAndLastDatesOfWeek {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *startOfPeriod;
    NSDate *endOfPeriod;
    NSTimeInterval interval;
    [cal rangeOfUnit:NSWeekCalendarUnit startDate:&startOfPeriod interval:&interval forDate:self];
    //startOfWeek holds now the first day of the week, according to locale (monday vs. sunday)
    
    endOfPeriod = [startOfPeriod dateByAddingTimeInterval:interval-1];
    // holds 23:59:59 of last day in week.
    return @[startOfPeriod, endOfPeriod];
}

- (NSDate*) dateByAddingDays:(int)days {
    NSDateComponents *dateCmp = [NSDateComponents new];
    dateCmp.day = days;
    
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateCmp toDate:[NSDate date] options:0];
}

- (NSDate*) dateByAddingWeeks:(int)weeks {
    NSDateComponents *dateCmp = [NSDateComponents new];
    dateCmp.week = weeks;
    
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateCmp toDate:[NSDate date] options:0];
}

- (NSDate*) dateByAddingMonths:(int)months {
    NSDateComponents *dateCmp = [NSDateComponents new];
    dateCmp.month = months;
    
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateCmp toDate:[NSDate date] options:0];
}

@end

@implementation NSArray (GHCategories)

- (BOOL) containsString:(NSString *)search {
    for (int i = 0; i < self.count; i++) {
        if (![[self objectAtIndex:i] isKindOfClass:[NSString class]]) continue;
        
        NSString *str = [self objectAtIndex:i];
        if ([str isEqualToString:search]) return true;
    }
    
    return false;
}

- (BOOL)containsSubstringInString:(NSString *)search {
    for (int i = 0; i < self.count; i++) {
        if (![[self objectAtIndex:i] isKindOfClass:[NSString class]]) continue;
        
        NSString *str = [self objectAtIndex:i];
        if ([str contains:search]) return true;
    }
    
    return false;
}

- (int)indexContainingString:(NSString *)search {
    for (int i = 0; i < self.count; i++) {
        if (![[self objectAtIndex:i] isKindOfClass:[NSString class]]) continue;
        if ([[self objectAtIndex:i] contains:search]) return i;
    }
    
    return -1;
}

- (BOOL)isStringArray {
    for (NSObject *obj in self) if (![obj isKindOfClass:[NSString class]]) return false;
    return true;
}

- (BOOL)isKindOfArray:(Class)aClass {
    for (NSObject *obj in self) if (![obj isKindOfClass:aClass]) return false;
    return true;
}

@end

@implementation NSNotificationCenter (GHCategories)

static NSMutableDictionary *notificationTable;

+ (void) post:(NSString*)notificationName {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil];
    });
}

+ (void) listen:(NSString *)notificationName withSelector:(SEL)selector withTarget:(id)target {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] addObserver:target selector:selector name:notificationName object:nil];
    });
}

+ (void) remove:(NSString *)notificationName fromTarget:(id)target {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] removeObserver:target name:notificationName object:nil];
    });
}

+ (void)removeAllFromTarget:(id)target {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] removeObserver:target];
    });
}

@end

@implementation UINavigationController (GHCategories)

- (void)presentViewControllerInNavController:(UIViewController *)view animated:(BOOL)animated completion:(void (^)(void))completion {
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:view] animated:animated completion:completion == nil ? nil : ^{
        completion();
    }];
}

@end

@implementation UILabel (GHCategories)

- (void)sizeLabelToRect:(CGRect)rect {
    // Set the frame of the label to the targeted rectangle
    self.frame = rect;
    
    NSInteger size = [self.text getFontSizeToFitSize:self.frame.size];
    self.font = [UIFont systemFontOfSize:size];
}



@end