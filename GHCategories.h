//
//  GHCategories.h
//
//  Created by Alex Martin on 11/26/13.
//  Copyright (c) 2013 Alex Martin. All rights reserved.
//
//  GHCategories is made as a drop-in category file for iOS development.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (GHCategories)

/*! Get the font size required to fit the specified rectangle */
-(NSInteger)getFontSizeToFitSize:(CGSize)size;

/*! Convert the string into a MD5 hash. */
-(NSString *)MD5;

/*! Convert the string into an sha1 hash. */
-(NSString *)sha1;

/*! Reverse the string. */
-(NSString *)reverse;

/*! Encode the string to URL. */
-(NSString *)URLEncode;

/*! Decode the URL into a string. */
-(NSString *)URLDecode;

/*! Strip whitespace from the beginning and end of the string */
-(NSString *)stringByStrippingWhitespace;

/*! Return the string between the specified character indexes. Example: "Hello" 2-4 would return "llo". */
-(NSString *)substringFrom:(NSInteger)from to:(NSInteger)to;

/*! Convert the specified string to propert capitalization. Example: "hElLO worLD" would return "Hello World". */
-(NSString *)CapitalizeFirst;

/*! Convert a string with raw mileage (i.e. 123000.0) to a eye-friendly string (123,000) */
- (NSString*) mileageToStr;

/*! Convert a string with readable mileage (i.e. 123,000) to a raw string (123000.0) */
- (NSString*) strToMileage;

/*! Convert underscores to camlecase style. Example: "hello_world" would return "helloWorld". */
-(NSString *)UnderscoresToCamelCase:(NSString*)underscores;

/*! Convert camelcase to underscores. Example: "helloWorld" would return "hello_world". */
-(NSString *)CamelCaseToUnderscores:(NSString *)input;

/*! Return the number of words in the specified string. */
-(NSUInteger)countWords;

/*! Check if the string contains the specified string. Example: "Hello World" contains "World". Case sensitive. */
-(BOOL)contains:(NSString *)string;

/*! Returns whether the string is blank or all whitespace. */
-(BOOL)isBlank;

/*! Remove the specified string from the current string */
- (NSString*) removeString:(NSString*)remove;

/*! Get the first fiew words of a string within the characters provided */
- (NSString*) getFirstWordsWithMaxCharacters:(int)maxChars;

/*! Strip all non-alphanumeric characters from the string */
- (NSString*) stringByStrippingSpecialCharacters;

@end

@interface NSUserDefaults (GHCategories)

/*! Save the specified object to the specified key */
+ (void)saveObject:(id)object forKey:(NSString *)key;

/*! Retreive the object stored for the specified key */
+ (id)retrieveObjectForKey:(NSString *)key;

/*! Delete the object stored for the specified key */
+ (void)deleteObjectForKey:(NSString *)key;

@end

@interface UIImage (GHCategories)

/*! Rotate the specified image by the number of degrees */
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;

/*! Create an image from the specified color */
+ (UIImage *)imageWithColor:(UIColor *)color;

@end

@interface UIView (GHCategories)

/*! Rotate the specified view by the number of degrees */
- (UIView*)viewRotatedByDegrees:(CGFloat)degrees;

/*! Resign first responder for this view, and make sure all subviews are resigned as well. Returns the view that was resigned or nil if none. */
- (UIView*) resignSubviewFirstResponders;

@end

@interface UIAlertView (GHCategories)

/*! Create and show an alert with the specified title and message, and an OK button */
+ (UIAlertView*) showOkAlertWithTitle:(NSString*)title andMessage:(NSString*)message;

/*! Show the alert view, and execute the completion block when the alert view is dismissed */
- (void)showWithCompletion:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completion;

@end

@interface NSDate (GHCategories)

/*! Format the date into the specified string. */
- (NSString *)formatWithString:(NSString *)format;

/*! Format the date into the specified style. */
- (NSString *)formatWithStyle:(NSDateFormatterStyle)style;

/*! Convert a future date into a representation of time from now. Example: "30 minutes from now". */
- (NSString *)distanceOfTimeInWords;

/*! Convert a future date into a text representation of time from the specified date. Example: "30 minutes from [date]". */
- (NSString *)distanceOfTimeInWords:(NSDate *)date;

/*! Get date objects for the start and end of today (00:00:01 - 23:59:59) and return in a 2-element array */
- (NSArray*) getFirstAndLastDatesOfDay;

/*! Get date objects for the start of the week and the end of the week and return in a 2-element array */
- (NSArray*) getFirstAndLastDatesOfWeek;

/*! Get date objects for the start of the month and the end of the month and return in a 2-element array */
- (NSArray*) getFirstAndLastDatesOfMonth;

/*! Add the specified number of days to the date */
- (NSDate*) dateByAddingDays:(int)days;

/*! Add the specified number of weeks to the date */
- (NSDate*) dateByAddingWeeks:(int)weeks;

/*! Add the specified number of months to the date */
- (NSDate*) dateByAddingMonths:(int)months;

@end

@interface NSArray (GHCategories)

/*! Check if the array contains the specified string */
- (BOOL) containsString:(NSString*)search;

/*! Check if any strings in the array contain the specified string */
- (BOOL) containsSubstringInString:(NSString*)search;

/*! Get the index in the array that contains the specified string. Returns -1 if no index. */
- (int) indexContainingString:(NSString*)search;

/*! Check if the array is composed entirely of string objects */
- (BOOL) isStringArray;

/*! Check if the array is composed entirely of the specified class */
- (BOOL) isKindOfArray:(Class)aClass;

@end

@interface NSNotificationCenter (GHCategories)

/*! Post the specified notification */
+ (void) post:(NSString*)notificationName;

/*! Remove the specified target from the observer list for the notification */
+ (void) remove:(NSString*)notificationName fromTarget:(id)target;

/*! Remove all notification listeners from the specified target */
+ (void) removeAllFromTarget:(id)target;

/*! Add the specified selector in the target as an observer for the notification */
+ (void) listen:(NSString*)notificationName withSelector:(SEL)selector withTarget:(id)target;

@end

@interface UINavigationController (GHCategories)

/*! Present the provided view controller inside of a new navigation controller */
- (void) presentViewControllerInNavController:(UIViewController*)view animated:(BOOL)animated completion:(void(^)(void))completion;

@end

@interface UILabel (GHCategories)

/*! Size the label to the specified rectangle */
- (void) sizeLabelToRect:(CGRect)rect;

@end