GHCategories
============

A collection of categories for use with iOS development. 

Please note: I have been building these categories over the last several months and they may contain snippets created by other people. 
Unfortunately, I haven't been as good about recording the sources of my code snippets as I am now. 
No infringement is intended; if you see code that is yours and want me to remove it or attribute it, create an issue on the repo with proof that the code is yours and I'll comply. 